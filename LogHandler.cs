﻿using log4net;

namespace ProfilePowerTools
{
    internal static class LogHandler
    {
        internal static readonly ILog Log = LogManager.GetLogger("[PPT]");

        internal static void Warn(string s)
        {
            Log.Warn(s);
        }
        
    }
}
