﻿using System;
using System.Threading;
using Buddy.Common;
using Buddy.ProfileBot;
using Buddy.Wildstar.Engine;
using Buddy.Wildstar.Engine.Profiles;

// ReSharper disable UseStringInterpolation
// ReSharper disable InvertIf

namespace ProfilePowerTools
{
    internal static class ProfileBotHandler
    {

        public static void Bounce()
        {

            if (!string.Equals(GameEngine.CurrentBot.Name, "Profile Bot"))
            {
                LogHandler.Log.Error("The 'Bounce' functionality only works with  the default 'Profile Bot'!");
                return;
            }
            LogHandler.Log.Warn("\n-----------------------------------------");
            LogHandler.Log.Info("Bot Name: ".PadLeft(20) + GameEngine.CurrentBot.Name);
            LogHandler.Log.Info("Current FilePath: ".PadLeft(20) + ProfileBotSettings.Instance.FilePath);
            LogHandler.Log.Info("Last Profile: ".PadLeft(20) + ProfileBotSettings.Instance.LastProfile);
            StopCurrentBot();
            ReloadLastProfile();
            StartCurrentBot();
        }

        public static void ReloadLastProfile()
        {
            if (GameEngine.BotPulsator.IsPulsing)
            {
                LogHandler.Warn("Cannot reload a profile while the bot is running");
                return;
            }
            var profile = ProfileLoader.LoadProfile(ProfileBotSettings.Instance.LastProfile);

            if (profile == null)
            {
                LogHandler.Log.Error("Could not reload profile: " + ProfileBotSettings.Instance.LastProfile);
                return;
            }
            profile.BuildReferences();
            LogHandler.Log.Info(string.Format("Profile {0} (version {1}) by {2} loaded!", profile.Name, profile.Version ?? new Version(0,0), profile.Author));
            GameEngine.CurrentProfile = profile;
        }

        public static void StopCurrentBot()
        {
            if (GameEngine.BotPulsator.IsPulsing)
            {
                GameEngine.BotPulsator.Stop();
                LogHandler.Log.Warn(string.Format("{0}{1}","Stopping Bot: ".PadLeft(20),GameEngine.CurrentBot.Name));
                while (GameEngine.BotPulsator.IsPulsing)
                {
                    Thread.Sleep(100);
                }
                LogHandler.Log.Warn("Bot Stopped");
            }
        }


        public static void StartCurrentBot()
        {
            if (!GameEngine.BotPulsator.IsPulsing)
            {
                GameEngine.BotPulsator.Start();
                LogHandler.Log.Warn(string.Format("{0}{1}","Starting Bot: ".PadLeft(20),GameEngine.CurrentBot.Name));
            }
        }

        public static void Bounce(Hotkey hotkey)
        {
            Bounce();   
        }
    }
}
