﻿using System.Linq;
using Buddy.Common;
using Buddy.Wildstar.Game;
// ReSharper disable UseStringInterpolation

namespace ProfilePowerTools
{
    public static class InfoDumpHelper
    {
        internal static void ShowInventory(Hotkey key)
        {
            GameManager.Memory.ClearCache();
            LogHandler.Log.Info("---------------- Inventory Dump ----------------");
            foreach (var item in GameManager.Inventory.Bags.Items)
            {
                LogHandler.Log.Warn(string.Format("[{0}] {1} - QTY: {2}", item.Id, item.Name, item.StackCount));
                LogHandler.Log.Info(string.Format("         Category: {0}", item.Info.CategoryName));
                LogHandler.Log.Info(string.Format("      Can Salvage: {0}", item.CanSalvage));
                LogHandler.Log.Info(string.Format("      Trade Skill: {0}", item.Info.Tradeskill));
                LogHandler.Log.Info(string.Format("      Req'd level: {0}", item.Info.Item2.RequiredLevel));
                LogHandler.Log.Info(string.Format("       Item Level: {0}", item.Info.Item2.PowerLevel));


                
                if (item.Info.OnActivateSpells.Any())
                {
                    foreach (var spell in item.Info.OnActivateSpells)
                    {
                        LogHandler.Log.Info(string.Format("       OnActivate: [ baseid: {0}] {1} spellid: {2} ", spell.BaseSpellId, spell.Name,spell.SpellId));
                    }
                }
                
                foreach (var stat in item.Info.ImplicitStats)
                {
                    LogHandler.Log.Info(string.Format("         Stat: {0}:{1}",stat.Property,stat.Value));
                }
            }
        }

        internal static void ShowBuffs(Hotkey key)
        {
            LogHandler.Log.Info(string.Format("Total Buffs for Player {0} : {1}", GameManager.LocalPlayer.Name, GameManager.LocalPlayer.Buffs.Count));
            foreach (var buff in GameManager.LocalPlayer.Buffs)
            {
                LogHandler.Log.Info(string.Format(" Buff: [ID: {0} SpellID: {3}] {1}  remaining: {2}s", buff.Id, buff.Name, buff.TimeLeft,buff.SpellId));
            }
        }

        public static void ShowTradeMats(Hotkey key)
        {
            GameManager.Memory.ClearCache();
            LogHandler.Log.Info("---------------- Trade Materials Dump ----------------");
            foreach (var item in GameManager.Inventory.Tradeskill.Materials)
            {
                LogHandler.Log.Warn(string.Format("[{0}] {1} - QTY: {2}", item.Id, item.ItemRecord.Name, item.Count));
            }
        }
    }
}
