﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Buddy.Common;
using Buddy.Coroutines;
using Buddy.Wildstar.BotCommon;
using Buddy.Wildstar.Engine;
using Buddy.Wildstar.Game;
using ModifierKeys = Buddy.Common.ModifierKeys;
// ReSharper disable UseStringInterpolation
// ReSharper disable ConvertPropertyToExpressionBody

namespace ProfilePowerTools
{
    public class ProfilePowerTools : CoroutinePulsable, IPlugin, IUIButtonProvider

    {
        
        public string Name { get { return "ProfilePowerTools"; } }
        public string Author { get { return "Brewer"; } }
        public Version Version { get { return new Version(0,2,0); } }
        public override bool CanBePulsed { get { return true; } }
        public bool Enabled { get; set; }

        public override async Task CoroutineImplementation()
        {
            //Sanities
            if (GameManager.LocalPlayer == null || !GameManager.LocalPlayer.IsValid ||
                GameManager.LocalPlayer.IsDead || !GameManager.LocalPlayer.IsAlive)
                return;
            if (GameManager.WorldLoadState != WorldLoadState.InWorld) return;
            if (GameManager.LocalPlayer.IsInCombat) return;

            if (Enabled && DialogWin.DoSoldierBuffs)
            {
                var unit = GameManager.Actors.Values.FirstOrDefault(
                    a => 
                    a.Distance <= 50f &&
                    a.ActivationTypes.Contains(ActivationType.Interact) &&
                    a.IsValid &&
                    a.Name == "Weapons Lockbox");
                if (unit != null)
                {
                    GameEngine.BotPulsator.Suspend(GameEngine.CurrentBot,TimeSpan.FromMinutes(5));
                    LogHandler.Warn("Found Soldier Lockbox to get");
                    if (unit.Distance > 24f)
                    {
                        LogHandler.Warn("Moving to lockbox");
                        await CommonBehaviors.MoveWithin(unit.Position, 10f, true, true);
                    }
                    else
                    {
                        LogHandler.Warn("Interacting w/Lockbox");
                        await CommonBehaviors.Interact(() => unit, 10f, true, 5000);
                        await Coroutine.Wait(5000, () => GameManager.LocalPlayer.IsCasting);
                        GameEngine.BotPulsator.Resume(GameEngine.CurrentBot);
                    }
                }
            }

            #region Salvage
            if (Enabled && DialogWin.DoSalvage)
            {
                var scrap = DialogWin.SalvageHousing 
                    ? GameManager.Inventory.Bags.Items.FirstOrDefault(e => e.CanSalvage && e.Info.CategoryName != "Housing" && e.Info.Item2.PowerLevel <= DialogWin.SalvageLevel) 
                    : GameManager.Inventory.Bags.Items.FirstOrDefault(e => e.CanSalvage && e.Info.Item2.PowerLevel <= DialogWin.SalvageLevel);
                if (scrap != null)
                {
                    scrap.Salvage();
                    LogHandler.Log.Warn(string.Format("Salvaging: [{0}]] {1}",scrap.Info.Item2.PowerLevel,scrap.Name));
                    await Coroutine.Wait(2000, () => GameManager.LocalPlayer.IsCasting);
                    return;
                }
            }

            #endregion

            #region Eating
            if (Enabled && DialogWin.KeepFed)
            {
                //if we already have 'Stuffed' return
                if (GameManager.LocalPlayer.Buffs.Any(e => e.Name == "Stuffed!")) return;

               //See if we have a stuffable food type in Inventory
                var food =
                    GameManager.Inventory.Bags.Items.FirstOrDefault(
                        e => e.Info.OnActivateSpells.Any(f => (f.BaseSpellId == 57784 || f.BaseSpellId == 57789 || f.BaseSpellId == 57794)));
                if (food == null) return;

                //We have food and need buff.
                
                GameEngine.BotPulsator.Suspend(GameEngine.CurrentBot, TimeSpan.FromMilliseconds(6000));
                LogHandler.Warn(string.Format("PPT: Eating Food: {0} with BaseSpellId of: {1}",food.Name,food.Info.OnActivateSpells.First().BaseSpellId));
                food.Use();
                await Coroutine.Wait(5000, () => GameManager.LocalPlayer.IsCasting);
                
            }
            #endregion
        }

      //  public override void Pulse() { }
        public override void OnRegistered() { }
        public override void OnUnregistered() { }
        

        private Hotkey _showInventory;
        private Hotkey _bounceKey;
        private Hotkey _showBuffs;
        private Hotkey _showTradeMats;
        private Hotkey _enableKey;
        protected Dialog DialogWin = Dialog.Instance;
        
        
        public void Initialize()
        {
            _bounceKey      = Hotkeys.Register("BounceProfileBot", Keys.F12, ModifierKeys.Control, ProfileBotHandler.Bounce);
            _showBuffs      = Hotkeys.Register("ShowBuffs", Keys.F2, ModifierKeys.Control | ModifierKeys.Alt, InfoDumpHelper.ShowBuffs);
            _showInventory  = Hotkeys.Register("ShowInventory", Keys.F1, ModifierKeys.Control | ModifierKeys.Alt,InfoDumpHelper.ShowInventory);
            _showTradeMats  = Hotkeys.Register("ShowTradeMats", Keys.F3, ModifierKeys.Control | ModifierKeys.Alt, InfoDumpHelper.ShowTradeMats);
            _enableKey      = Hotkeys.Register("EnableKey",Keys.F4, ModifierKeys.Control | ModifierKeys.Alt, ToggleMe);

            LogHandler.Log.Warn("PPT: Initialized Hotkey [CTRL][ALT]-F12 for Profile Bot Stop/Reload/Start");
            LogHandler.Log.Warn("PPT: Initialized Hotkey [CTRL][ALT]-F1 for a Dump of Current Inventory");
            LogHandler.Log.Warn("PPT: Initialized Hotkey [CTRL][ALT]-F2 for a Dump of Current Buffs");
            LogHandler.Log.Warn("PPT: Initialized Hotkey [CTRL][ALT]-F3 for a Dump of Current Trade Materials");
            LogHandler.Log.Warn("PPT: Initialized Hotkey [CTRL][ALT]-F4 to enable/disable plugins pulsator (handles eating,gathering, etc)");
            GameEngine.BotPulsator.RegisterForPulse(this);
            Enabled = true;
        }

        private void ToggleMe(Hotkey obj)
        {
            Enabled = !Enabled;
            LogHandler.Log.Warn(string.Format("[PPT] Plugin Pulse task is: {0}",Enabled));
        }

        public void Uninitialize()
        {
            GameEngine.BotPulsator.UnregisterForPulse(this);
            Hotkeys.Unregister(_showBuffs);
            Hotkeys.Unregister(_bounceKey);
            Hotkeys.Unregister(_showInventory);
            Hotkeys.Unregister(_showTradeMats);
            Hotkeys.Unregister(_enableKey);
        }

        #region IUIButtonProvider
        public string ButtonText
        {
            get { return "PPT Settings"; }
        }
        public void OnButtonClicked(object sender)
        {
            DialogWin.Toggle();
        }
        #endregion

    }
}
