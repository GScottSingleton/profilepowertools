﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Markup;
using Buddy.Common;
using MahApps.Metro.Controls;
using Buddy.Wildstar.Game;
// ReSharper disable ConvertPropertyToExpressionBody

namespace ProfilePowerTools
{
    public class Dialog : JsonSettings
    {
        private readonly Flyout _mainFlyout;
        private static MetroWindow MainWindow { get { return (MetroWindow)Application.Current.MainWindow; } }
        private static readonly Lazy<Dialog> s_lzDialog = new Lazy<Dialog>(()=> new Dialog());
        public static Dialog Instance { get { return s_lzDialog.Value; } }

        public Dialog() 
            : base(GetSettingsFilePath(SettingsPath,"ProfilePowerTools",GameManager.LocalPlayer.Name,"Settings.json"))
        {
            using (var fs = new FileStream(@"Plugins\ProfilePowerTools\dialog.xaml", FileMode.Open))
            {
                _mainFlyout = (Flyout)XamlReader.Load(fs);
                _mainFlyout.DataContext = this;
            }
        }

        #region Show/Hide Dialog methods
        public void Toggle()
        {
            if (_mainFlyout.IsOpen)
                Hide();
            else
                Show();
        }
        private void Add()
        {
            if (!MainWindow.Flyouts.Items.Contains(_mainFlyout))
                MainWindow.Flyouts.Items.Add(_mainFlyout);
        }
        private void Remove()
        {
            if (MainWindow.Flyouts.Items.Contains(_mainFlyout))
                MainWindow.Flyouts.Items.Remove(_mainFlyout);
        }
        private void Show()
        {
            if (!MainWindow.Flyouts.Items.Contains(_mainFlyout))
                Add();

            _mainFlyout.IsOpen = true;
        }
        private void Hide()
        {
            _mainFlyout.IsOpen = false;
        }
        #endregion

        #region two-way bound fields

        private bool _keepFed;
        public bool KeepFed
        {
            get { return _keepFed; }
            set
            {
                if (value.Equals(_keepFed)) return;
                _keepFed = value;
                NotifyPropertyChanged(()=>KeepFed);
                Save();
            }
        }

        private bool _doSalvage;
        public bool DoSalvage
        {
            get { return _doSalvage; }
            set
            {
                if (value.Equals(_doSalvage)) return;
                _doSalvage = value;
                NotifyPropertyChanged(()=>DoSalvage);
                Save();
            }
        }

        private bool _salvageHousing;

        public bool SalvageHousing
        {
            get { return _salvageHousing; }
            set
            {
                if (value.Equals(_salvageHousing)) return;
                _salvageHousing = value;
                NotifyPropertyChanged(()=>SalvageHousing);
                Save();
            }
        }

        private int _salvageLevel;

        public int SalvageLevel
        {
            get { return _salvageLevel;}
            set
            {
                if (value.Equals(_salvageLevel)) return;
                _salvageLevel = value;
                NotifyPropertyChanged(()=>SalvageLevel);
                Save();
            }
        }

        private bool _doSoldierBuff;

        public bool DoSoldierBuffs
        {
            get { return _doSoldierBuff;}
            set
            {
                if (value.Equals(_doSoldierBuff)) return;
                _doSoldierBuff = value;
                NotifyPropertyChanged(()=>DoSoldierBuffs);
                Save();
            }
        }



        #endregion

    }
}
